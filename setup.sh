#!/bin/bash
BUILD=1
FRESH=1

while [[ $# -gt 0 ]]; do
	case $1 in 
		-b|--build)
		 BUILD=0
		 shift
		 ;;
		-f|--fresh)
		 FRESH=0
		 shift
		 ;;
		-*|--*)
		 echo "Unkon option $1"
		 exit 1
		 ;;
		*)
		 POSITIONAL_ARGS+=("$1")
		 shift
		 ;;
	esac
done

if [[ FRESH -eq 0 ]];
then
	for file in dl-poly dlpoly-py
	do
		if [ -d $file ];
		then
			rm -rf $filr
		fi
	done
fi

if [ -d dl-poly ];
then
	echo "Found dl-poly, you can delete and reinstall with -f"
else
	git clone https://gitlab.com/ccp5/dl-poly.git
fi

if [ -d dlpoly-py ];
then
	echo "Found dlpoly-py, you can delete and reinstall with -f"
else
	git clone https://gitlab.com/drFaustroll/dlpoly-py
fi

if [[ BUILD -eq 0 ]];
then
	cd dl-poly
	bash utils/build-mpi-pure.sh
fi

		


