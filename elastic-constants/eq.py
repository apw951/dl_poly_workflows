from dlpoly import DLPoly

import argparse
from ase.io import write
from ase.lattice.cubic import FaceCenteredCubic
from pathlib import Path
from time import time

def run(stem, temp):
    dlPoly = DLPoly(exe=args.exe,control=f"{in_dir}/CONTROL", 
                    config=f"{in_dir}/CONFIG",
                    field=f"{in_dir}/FIELD", 
                    workdir=f"{stem}", 
                    output=f"{stem}/OUTPUT")
    dlPoly.control['temperature'] = (temp,'K')
    dlPoly.control['random_seed'] = (100,temp,round(time()*1000))
    dlPoly.run(numProcs = args.np)

parser = argparse.ArgumentParser(description='Test DL_POLY correlations functions')

parser.add_argument('--exe', dest='exe', type=str, default=None, help='location of DLPOLY.Z executable')
parser.add_argument('-i', dest='input', type=str, default="./", help='location of inputs')
parser.add_argument('-np', dest='np', type=int, default=1, help='number of processes')
parser.add_argument('-r', dest='r', type=int, default=1, help='number of repeats')
parser.add_argument('-temperature',dest='T',type=float,default=400.0,help='temperature, K')
parser.add_argument('-o', dest='output', type=str, default='out', help='output directory stem')
parser.add_argument('-cells',dest='cells',type=int,default=5,help='FCC unit cell repetition in each direction (x,y,z)')

args = parser.parse_args()

out_dir = Path(args.output)
in_dir = Path(args.input)

if out_dir.is_dir():
    out_dir.rmdir()

if not in_dir.is_dir():
    in_dir.mkdir()

atoms = FaceCenteredCubic(directions=[[1, 0, 0], [0, 1, 0], [0, 0, 1]],
                          symbol='Ar',
                          size=(args.cells, args.cells, args.cells),
                          pbc=True)

write(f"{in_dir}/CONFIG",atoms,format='dlp4')

f = """lj
units eV
molecular types 1
1 argon atoms
nummols       {}
atoms 1
Ar             39.95      0.0     1     0    1
finish
vdw        1
Ar      Ar      lj  0.01032   3.40
close
""".format(len(atoms))

file = open(f"{in_dir}/FIELD","w")
file.write(f)
file.close()

c = """title DL_POLY: ar

io_file_config CONFIG
io_file_field FIELD
io_file_statis STATIS
io_file_revive NONE

io_statis_yaml ON
print_frequency  1000 steps
stats_frequency  1000 steps

stack_size 10000 steps

vdw_cutoff  12.0 ang
vdw_method direct
cutoff  12.0 ang

ensemble npt
ensemble_method hoover
ensemble_thermostat_coupling  0.5 ps
ensemble_barostat_coupling  1 ps

time_run  100000 steps
time_job  10000000.0 s
time_close  100.0 s
timestep  0.001 ps
pressure_hydrostatic  0 katm
"""

file = open(f"{in_dir}/CONTROL","w")
file.write(c)
file.close()

if args.r == 1:
    run(args.output, args.T)
else:
    for r in range(args.r, args.T):
        run(args.output+f"-{r}")
