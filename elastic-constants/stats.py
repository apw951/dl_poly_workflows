import argparse
from pathlib import Path
from dlpoly import DLPoly
from time import time

c = """title DL_POLY: ar

io_file_config CONFIG-eq
io_file_field FIELD
io_file_statis STATIS
io_file_revive NONE

io_statis_yaml ON
print_frequency  10000 steps
stats_frequency  100 steps

stack_size 10000 steps

correlation_observable [  s-s ] 
correlation_block_points [  10000 ] 
correlation_dump_frequency 100000 steps

vdw_cutoff  12.0 ang
vdw_method direct
cutoff  12.0 ang

elastic_constants On

ensemble nvt
ensemble_method hoover
ensemble_thermostat_coupling  0.1 ps

time_run  1000000 steps
time_equilibration  0 steps
time_job  10000000.0 s
time_close  100.0 s
timestep  0.001 ps
pressure_hydrostatic  0 katm"""

def run(stem, temp, np):
    stem = out_dir
    dlPoly = DLPoly(exe=args.exe,control=f"{in_dir}/CONTROL-prod", 
                    config=f"{in_dir}/CONFIG-eq",
                    field=f"{in_dir}/FIELD", 
                    workdir=f"{stem}", 
                    output=f"{stem}/OUTPUT")
    dlPoly.control['temperature'] = (temp,'K')
    dlPoly.control['random_seed'] = (100,temp,round(time()*1000))
    dlPoly.run(numProcs = np) 

parser = argparse.ArgumentParser(description='Test DL_POLY correlations functions')

parser.add_argument('--exe', dest='exe', type=str, default=None, help='location of DLPOLY.Z executable')
parser.add_argument('-i', dest='input', type=str, default="./", help='location of inputs')
parser.add_argument('-np', dest='np', type=int, default=1, help='number of processes')
parser.add_argument('-r', dest='r', type=int, default=1, help='number of repeats')
parser.add_argument('-temperature',dest='T',type=float,default=400.0,help='temperature, K')
parser.add_argument('-o', dest='output', type=str, default='out', help='output directory stem')
args = parser.parse_args()

out_dir = Path(args.output)
in_dir = Path(args.input)

if not out_dir.is_dir() or not in_dir.is_dir():
    print(f"I/O dir error")
    exit

file = open(f"{in_dir}/CONTROL-prod","w")
file.write(c)
file.close()

if args.r == 1:
    run(args.output+f"-T{args.T}-prod", args.T, args.np)
else:
    for r in range(args.r):
        run(args.output+f"-T{args.T}-{r}-prod", args.T, args.np)