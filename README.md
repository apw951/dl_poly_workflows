# DL_POLY_workflows

### A few useful workflows for DL_POLY

#### Viscosity measurements

- Ar example that can reproduce [NIST data](https://webbook.nist.gov/chemistry/fluid/) quite well
- [viscosity-thermalconductivity/](https://gitlab.com/apw951/dl_poly_workflows/-/tree/main/viscosity-thermalconductivity?ref_type=heads)

- [LiF example](https://gitlab.com/apw951/dl_poly_workflows/-/tree/main/viscosity-thermalconductivity/LiF?ref_type=heads)

#### Thermal conductivity measurements

- Ar example that can reproduce [NIST data](https://webbook.nist.gov/chemistry/fluid/) quite well
- [viscosity-thermalconductivity/](https://gitlab.com/apw951/dl_poly_workflows/-/tree/main/viscosity-thermalconductivity?ref_type=heads)

- [LiF example](https://gitlab.com/apw951/dl_poly_workflows/-/tree/main/viscosity-thermalconductivity/LiF?ref_type=heads)

- [LiF with mass flux correction](https://gitlab.com/apw951/dl_poly_workflows/-/tree/main/viscosity-thermalconductivity/LiF/intrinsic-thermal-conductivity?ref_type=heads) (see [arXiv 2409.03775](https://arxiv.org/abs/2409.03775))

#### Elastic constants

- [Ar example](https://gitlab.com/apw951/dl_poly_workflows/-/tree/main/elastic-constants?ref_type=heads)

#### Currents

- [Ar longitudinal and transverse currents](https://gitlab.com/apw951/dl_poly_workflows/-/tree/main/currents?ref_type=heads)

#### Rigid body correlations

- [CH4 Velocity](https://gitlab.com/apw951/dl_poly_workflows/-/tree/main/rigid_correlations/CH4?ref_type=heads)

- [SF6 Velocity](https://gitlab.com/apw951/dl_poly_workflows/-/tree/main/rigid_correlations/SF6?ref_type=heads) reproducing Brodka, A. and Zerda, T.W., 1992. A molecular dynamics simulation of sulphur hexafluoride. Molecular Physics, 76(1), pp.103-112.
