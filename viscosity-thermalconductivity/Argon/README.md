### Viscosity and Thermal conductivity measurements in DL_POLY

- Python files to run the actual work (for one temperature and n repeats) 
- N = 500 is typically enough for good measurements in Argon

#### Schedulers

- UGE Array job example [sub-array.uge](https://gitlab.com/apw951/dl_poly_workflows/-/blob/main/viscosity-thermalconductivity/sub-array.uge)
- SLURM Array job example [sub-array.sbatch](https://gitlab.com/apw951/dl_poly_workflows/-/blob/main/viscosity-thermalconductivity/sub-array.sbatch)
