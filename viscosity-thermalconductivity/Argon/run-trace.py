#!/usr/bin/env python3

from dlpoly import DLPoly
from dlpoly.correlations import Correlations

import numpy as np
import argparse

from time import time
import subprocess

from os import path

parser = argparse.ArgumentParser(description='Test DL_POLY correlations functions')

parser.add_argument('--exe', dest='exe', type=str, default=None, help='location of DLPOLY.Z executable')
parser.add_argument('--config-dir', dest='dir', type=str, default="./", help='location of Ar test files')
parser.add_argument('-np', dest='np', type=int, default=1, help='number of processes')
parser.add_argument('-r', dest='r', type=int, default=1, help='number of repeats')
parser.add_argument('-temperature',dest='T',type=float,default=400.0,help='temperature, K')
parser.add_argument('-clean',action='store_true')

args = parser.parse_args()

repeats = args.r

viscosity = np.zeros((repeats))
kinematicViscosity = np.zeros((repeats))
thermalCond = np.zeros((repeats))
thermalCondComponents = np.zeros((repeats,9))

T = round(args.T,2)

print("Process T = {}, equilibriation".format(T))

if (path.isfile("data/T{}/CONFIG-eq".format(T))):
    print("Data already exists, using it")
else:

    # build a dlpoly object with CONTROL, CONFIG, and FIELD as inputs
    dlPoly = DLPoly(exe=args.exe,control=f"{args.dir}CONTROL", config=f"{args.dir}CONFIG",
                field=f"{args.dir}FIELD", workdir="data/T{}".format(T), output="data/OUTPUT-{}".format(T))

    # set temperature and seed
    dlPoly.control['temperature'] = (T,'K')
    dlPoly.control['random_seed'] = (100,1,round(time()*1000))
    dlPoly.run(numProcs = args.np)
    # copy the equi configs over to the given temperature directory
    subprocess.run(["cp", "data/T{}/REVCON".format(T),"data/T{}/CONFIG-eq".format(T)])

for r in range(repeats):
    # main runs for statistics
    print("Process T = {}, production run {}".format(T,r))

    # first check if data exists, that we can skip on a re-run after partial completion
    archive = path.isfile("T{}-{}-prod.tar.xz".format(T,r))
    if ( path.isfile("data/T{}-{}-prod/thermal-conductivity".format(T,r))  or archive ):
        print("Data already exists, skipping")

        # uncompress data if required
        if (archive):
            subprocess.run(["tar","-xvJf","T{}-{}-prod.tar.xz".format(T,r)])
        
        # get the viscosity/ thermal-conductivity from already existing runs
        viscosity[r] = np.loadtxt("data/T{}-{}-prod/viscosity".format(T,r))
        kinematicViscosity[r] = np.loadtxt("data/T{}-{}-prod/kinematic-viscosity".format(T,r))
        thermalCond[r] = np.loadtxt("data/T{}-{}-prod/thermal-conductivity".format(T,r))
        thermalCondComponents[r,:] = np.loadtxt("data/T{}-{}-prod/thermal-conductivity-components".format(T,r))

        if (archive):
            subprocess.run(["rm","-r","data/T{}-{}-prod".format(T,r)])
	
        continue        

    # dlpoly object taking the production control (NVE for the stats)
    #   also takes the equilibration config file
    dlPoly = DLPoly(exe=args.exe,control=f"{args.dir}CONTROL-prod", config=f"data/T{T}/CONFIG-eq",
                field=f"{args.dir}FIELD", workdir="data/T{}-{}-prod".format(T,r), output="data/OUTPUT-{}-{}".format(T,r))
    # set temperature and seed
    dlPoly.control['temperature'] = (T,'K')
    dlPoly.control['random_seed'] = (r,1,round(time()*1000))
    # execute dlpoly
    dlPoly.run(numProcs = args.np)
    # load in the correlations to python 
    dlPoly.load_correlations()

    derived = dlPoly.correlations.derived
    # extract the derived data (if it is there!)
    for d in derived:
        if 'viscosity' in d.keys():
            viscosity[r] = d['viscosity']['value'] * ((101325 * 1e3)) * 1e-12 / 1e-6 # convert to: micro Pa s, from K atm s 
        if 'kinematic-viscosity' in d.keys():
            kinematicViscosity[r] = d['kinematic-viscosity']['value'] * ((101325 * 1e3)) * 1e-12 / 1e-6 # convert to: micro Pa s, from K atm s 
        if 'thermal-conductivity' in d.keys():
            thermalCond[r] = d['thermal-conductivity']['value']
            if 'components' in d['thermal-conductivity'].keys():
                thermalCondComponents[r,:] = d['thermal-conductivity']['components']


    # compress various job files from this run, to save space
    if (args.clean):
        subprocess.run(["rm", "data/T{}-{}-prod/REVIVE".format(T,r)])
        subprocess.run(["rm","data/T{}-{}-prod/CONFIG-eq".format(T,r)])
        np.savetxt("data/T{}-{}-prod/viscosity".format(T,r),np.array([viscosity[r]]))
        np.savetxt("data/T{}-{}-prod/thermal-conductivity".format(T,r),np.array([thermalCond[r]]))
        subprocess.run(["tar","-cvJf","T{}-{}-prod.tar.xz".format(T,r),"data/T{}-{}-prod/".format(T,r)])
        subprocess.run(["rm","-r","data/T{}-{}-prod".format(T,r)])

# write out data
np.savetxt("viscosity-{}".format(T), viscosity)
np.savetxt("kinematic-viscosity-{}".format(T), kinematicViscosity)

np.savetxt("thermal-conductivity-{}".format(T), thermalCond)
np.savetxt("thermal-conductivity-components-{}".format(T), thermalCondComponents)