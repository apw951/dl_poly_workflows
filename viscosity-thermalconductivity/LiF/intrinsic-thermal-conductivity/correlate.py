import numpy as np
from typing import Iterable
from glob import glob
from tqdm import tqdm

def integrate(x: Iterable[float], dt: float) -> float:
    return np.sum(x*dt)

def correlate(
    x: Iterable[float], y: Iterable[float], *, fft: bool = True
) -> Iterable[float]:
    """
    Direct correlation of x and y. If fft uses np.correlate in full mode.
    """
    n = min(len(x), len(y))
    if fft:
        cor = np.correlate(x, y, "full")
        cor = cor[len(cor) // 2 :]
        return cor / (n - np.arange(n))
    cor = np.zeros(n)
    for j in range(n):
        for i in range(n - j):
            cor[j] += x[i] * y[i + j]
        cor[j] /= n - j
    return cor

ev = 1.602176634e-19
ps = 1e-12
ang = 1e-10
kb = 1.380649e-23
hf_conv = ev / (ps * ang**2)
mf_conv = 1.6605402e-27 / (ps * ang**2)

tau = 500

for sys in ["LiF", "KCl"]:
    lambdas = []
    runs = [int(s.split("-")[-1]) for s in glob(f"{sys}/HEATFLUX-*")]
    for r in tqdm(runs):
        dt = 0.001 * 10 * ps
        with open(f"{sys}/HEATFLUX-{r}", "r") as file:
            lines = file.readlines()
            hf = np.zeros((len(lines), 3))
            mf = np.zeros((len(lines), 3))
            v = np.zeros(len(lines))
            t = np.zeros(len(lines))
            for (i, line) in enumerate(lines):
                columns = line.split()
                hf[i, :] = [float(f) for f in columns[3:6]]
                mf[i, :] = [float(f) for f in columns[7:]]
                t[i] = float(columns[1])
                v[i] = float(columns[2]) * ang**3
            hf *= hf_conv
            mf *= mf_conv

            L11 = dt*sum(correlate(mf[:, 0], mf[:, 0])[:tau]+correlate(mf[:, 1], mf[:, 1])[:tau] + correlate(mf[:, 2], mf[:, 2])[:tau])
            Lq1 = dt*sum(correlate(hf[:, 0], mf[:, 0])[:tau]+correlate(hf[:, 1], mf[:, 1])[:tau] + correlate(hf[:, 2], mf[:, 2])[:tau])
            Lqq = dt*sum(correlate(hf[:, 0], hf[:, 0])[:tau]+correlate(hf[:, 1], hf[:, 1])[:tau] + correlate(hf[:, 2], hf[:, 2])[:tau])

            lambdas.append( (Lqq - (Lq1**2)/L11) * np.mean(v)/(3.0*kb * np.mean(t)**2))

    l = np.mean(lambdas)
    print(f"{sys}: {l}")

