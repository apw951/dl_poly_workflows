from ase.build import bulk
from ase.io import write
import argparse

parser = argparse.ArgumentParser()

parser.add_argument("-a", action="store", dest="a", type=float, required=True)
parser.add_argument("-salt", action="store", dest="salt", type=str, required=True)
parser.add_argument("-repeat", action="store", dest="repeat", type=int, default=4)
args = parser.parse_args()

a = args.a*2
write("CONFIG", bulk(args.salt, crystalstructure="rocksalt", a=a, b=a, c=a, cubic=True).repeat(args.repeat), format="dlp4")
