#!/usr/bin/env python3
import argparse
from ase.io import write, read
from ase import Atoms
from ase.lattice.cubic import FaceCenteredCubic
from io import StringIO

parser = argparse.ArgumentParser(description='Test DL_POLY correlations functions')

parser.add_argument('-cells',dest='cells',type=int,default=5,help='FCC unit cell repetition in each direction (x,y,z)')

args = parser.parse_args()

size = args.cells

atoms = FaceCenteredCubic(directions=[[1, 0, 0], [0, 1, 0], [0, 0, 1]],
                          symbol='Ar',
                          size=(size, size, size),
                          latticeconstant=3.5,
                          pbc=True)

for i in range(len(atoms)):
    if(i % 2 == 0):
        atoms[i].symbol = 'Li'
    else:
        atoms[i].symbol = 'F'

write("CONFIG",atoms,format='dlp4')

f = """DL-POLY : Lithium-Flourine
units eV
molecular types 1
LiF
nummols     {} 
atoms 2
Li           6.941        1    1     0    1
F            18.998      -1    1     0    1
finish
vdw        3
Li     Li        buck    98.92   0.299  0.04557
Li     F         buck    228.99   0.299  0.49936
F      F         buck    420.48  0.299  9.0509
close
""".format(int(len(atoms)/2))

file = open("FIELD","w")
file.write(f)
file.close()


