import numpy as np

class History:

    def __init__(self, source: str):
        
        self.source = source
        self.data = None

        f = open(self.source)

        self.title = f.readline()

        params = f.readline().split()

        self.level = int(params[0])
        self.nAtoms = int(params[2])
        self.nFrames = int(params[3])

    def load_frame(self, time_step: int):

        f = open(self.source)

        title = f.readline()

        params = f.readline().split()

        level = int(params[0])
        nAtoms = int(params[2])
        nFrames = int(params[3])

        if (time_step < 0 or time_step > nFrames-1):

            self.data = None
            return

        self.data = np.zeros((nAtoms, 3 + level*3))

        m = 0

        while m < nFrames:

            if (m == time_step):

                # header
                next(f)
                next(f)
                next(f)
                next(f)

                # now atoms

                n = 0

                while n < nAtoms:

                    atomHeader = f.readline().split()

                    aid = int(atomHeader[1])-1

                    if (level >= 0):
                        self.data[aid, 0:3] = np.array(f.readline().split()).astype(float)
                    
                    if (level >= 1):
                        self.data[aid, 3:6] = np.array(f.readline().split()).astype(float)
                    
                    if (level == 2):
                        self.data[aid, 6:9] = np.array(f.readline().split()).astype(float)
                        
                    n += 1
                
                return

            else:
                
                for i in range(4+nAtoms*(1+level+1)):
                    next(f)
                
            m += 1


    def load_yaml(self):

        f = open(self.source)
        title = f.readline()

        self.data = {"title" : title.strip()}

        params = f.readline().split()

        self.data["level-key"] = int(params[0])
        self.data["periodic-key"] = int(params[1])
        self.data["n-atoms"] = int(params[2])
        self.data["n-frames"] = int(params[3])
        self.data["records"] = int(params[4])
        self.data["frames"] = []

        m = 0

        while m < data["n-frames"]:

            header = f.readline().split()
            cell1 = f.readline().split()
            cell2 = f.readline().split()
            cell3 = f.readline().split()

            step = header[1]
            dt = header[5]
            time = header[6]

            if (m == 0):
                
                self.data["timestep"] = float(dt)
                self.data["cell a"] = cell1
                self.data["cell b"] = cell2
                self.data["cell c"] = cell3

            # now atoms

            n = 0

            frame = []

            while n < self.data["n-atoms"]:

                atom = {}

                atomHeader = f.readline().split()

                atom["species"] = atomHeader[0]
                atom["id"] = int(atomHeader[1])
                atom["mass (amu)"] = float(atomHeader[2])
                atom["charge (e)"] = float(atomHeader[3])
                atom["displacement"] = float(atomHeader[4])

                if (self.data["level-key"] >= 0):
                    atom["position"] = f.readline().split()
                
                if (self.data["level-key"] >= 1):
                    atom["velocity"] = f.readline().split()
                
                if (self.data["level-key"] == 2):
                    atom["force"] = f.readline().split()
                    
                n += 1

                frame.append(atom)
            
            self.data["frames"].append(frame)

            m += 1



    def load_array(self):

        f = open(self.source)

        title = f.readline()

        params = f.readline().split()

        level = int(params[0])
        nAtoms = int(params[2])
        nFrames = int(params[3])

        self.data = np.zeros((nFrames, nAtoms, 3 + level*3))
        times = np.zeros(nFrames)

        m = 0

        while m < nFrames:

            header = f.readline().split()
            cell1 = f.readline().split()
            cell2 = f.readline().split()
            cell3 = f.readline().split()

            step = header[1]
            dt = header[5]
            time = header[6]

            times[m] = time

            # now atoms

            n = 0

            while n < nAtoms:

                atomHeader = f.readline().split()

                aid = int(atomHeader[1])-1

                if (level >= 0):
                    self.data[m, aid, 0:3] = np.array(f.readline().split()).astype(float)
                
                if (level >= 1):
                    self.data[m, aid, 3:6] = np.array(f.readline().split()).astype(float)
                
                if (level == 2):
                    self.data[m, aid, 6:9] = np.array(f.readline().split()).astype(float)
                    
                n += 1

            m += 1
