from ase.io import read, write
import argparse

parser = argparse.ArgumentParser(description='Rescale atoms')

parser.add_argument('-oc', dest='output', type=str, default="CONFIG_RESCALED", required=False, help='location of rescaled config')
parser.add_argument('CONFIG', type=str, help='location of input configuration')
parser.add_argument('-rho', dest='rho', type=float, required=False, help="Required density, g/cm^3")
args = parser.parse_args()

atoms = read(args.CONFIG, format="dlp4")
mass = sum(atoms.get_masses()) * 1.66054e-24
a = (mass/(args.rho*(1e-8)**3))**(1.0/3.0)

atoms.set_cell([(a, 0.0, 0.0), (0.0, a, 0.0), (0.0, 0.0, a)], scale_atoms=True)
write(args.output, atoms, format="dlp4")
