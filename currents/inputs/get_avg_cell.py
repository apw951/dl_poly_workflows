KEY="Average cell vectors"
from typing import TextIO, List
from pathlib import Path

def extract_cell_vector(output: TextIO) -> tuple[float, float, float]:
    return tuple([float(v) for v in next(output).split()[0:3]])

def extract_cell_vectors(output: Path) -> List[tuple[float, float, float]]:
    a = None
    b = None
    c = None
    with open("OUTPUT", "r") as output:
        for line in output:
            if KEY in line:
                a, b, c  = [extract_cell_vector(output) for i in range(3)]
    cell = [a,b,c]
    if any(cell) == None:
        raise ValueError("Cell vectors not extracted") 
    return [a,b,c]   

print(extract_cell_vectors(Path("OUTPUT"))) 
