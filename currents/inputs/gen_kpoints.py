import numpy as np

from argparse import ArgumentParser

arg_parse = ArgumentParser()
arg_parse.add_argument("--config", dest="config", type=str, default="CONFIG")
arg_parse.add_argument("-nk", dest="nk", type=int, required=False)
args = arg_parse.parse_args()

with open(args.config, "r") as config:
    title = config.readline(); 
    n = int(config.readline().split()[-1]);
    cv = float(config.readline().split()[0])

kmin = np.pi / cv
kmax = 2.0*np.pi / ( (cv**3)/n )**(1.0/3.0)
if args.nk is None:
    nk = int(kmax/kmin)
else:
    nk = args.nk

kd = (kmax-kmin)/(nk-1)

kpoints = np.array([i*kd+kmin for i in range(nk)])

with open("KPOINTS", "w") as KPOINTS:
    KPOINTS.write(f"{nk}\n")
    for k in kpoints:
        KPOINTS.write(f"0.0 0.0 {k}\n")
