from ase.io import read, write
import argparse

from get_avg_cell import *

parser = argparse.ArgumentParser(description='Rescale atoms')

parser.add_argument('-o', dest='output', type=str, required=False, help='location of output file to extract cell vectors')
parser.add_argument('-oc', dest='config', type=str, default="CONFIG_RESCALED", required=False, help='location of rescaled config')
parser.add_argument('-ic', dest='input', type=str, required=True, help='location of input configuration')
parser.add_argument('-a', dest='a', type=float, required=False, help="cell vector a length")
parser.add_argument('-b', dest='b', type=float, required=False, help="cell vector b length")
parser.add_argument('-c', dest='c', type=float, required=False, help="cell vector c length")
args = parser.parse_args()

atoms = read(args.input, format="dlp4")
if all(arg != None for arg in [args.a, args.b, args.c]): 
    atoms.set_cell([(args.a, 0.0, 0.0), (0.0, args.b, 0.0), (0.0, 0.0, args.c)],scale_atoms=True)
elif args.output != None:
    cell = extract_cell_vectors(args.output)
    atoms.set_cell(cell,scale_atoms=True)
else:
    raise ValueError("Must specify either cell vectors or an OUTPUT file to extract them from")
write(args.config, atoms, format="dlp4")
